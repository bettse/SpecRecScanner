//
//  ViewController.swift
//  SpecRecScanner
//
//  Created by Eric Betts on 5/18/17.
//  Copyright © 2017 Eric Betts. All rights reserved.
//

import UIKit
import CoreBluetooth
import BadgeSwift

var manager: CBCentralManager? = nil

enum SpecState {
    case none
    case idle
    case recording
    case snapcode
    case pairing
}

class ViewController: UIViewController, CBCentralManagerDelegate {
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    @IBOutlet weak var badge: BadgeSwift!
    var timeout: Timer?
    var state: SpecState = .none

    override func viewDidLoad() {
        super.viewDidLoad()
        manager = CBCentralManager(delegate: self, queue: nil)
        self.label.text = ""
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch (central.state) {
        case CBManagerState.poweredOff:
            break
        case CBManagerState.poweredOn:
            startScan()
            break
        default:
            break
        }
    }
    
    func startScan(){
        if let central = manager {
            let spectacleService = CBUUID(string: "3e400001-b5a3-f393-e0a9-e50e24dcca9e")
            central.scanForPeripherals(withServices: [spectacleService], options: [CBCentralManagerScanOptionAllowDuplicatesKey: true])
            self.spinner.startAnimating()
        }
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        
        parse(advertisementData["kCBAdvDataManufacturerData"] as! Data)
        
        if let name = peripheral.name {
            self.badge.text = name
        }
        
        updateUI()
    }
    
    func updateUI() {
        var newState = ""
        switch (self.state) {
        case .idle:
            newState = "Idle"
            self.badge.isHidden = false
            self.badge.badgeColor = UIColor.green
            break
        case .pairing:
            newState = "Pairing"
            self.badge.isHidden = false
            self.badge.badgeColor = UIColor.yellow
            break
        case .snapcode:
            newState = "Scanning snapcode"
            self.badge.isHidden = false
            self.badge.badgeColor = UIColor.orange
            break
        case .recording:
            newState = "Recording"
            self.badge.isHidden = false
            self.badge.badgeColor = UIColor.red
            break
        case .none:
            newState = ""
            self.spinner.stopAnimating()
            return
        }
        
        print(newState)
        self.label.text = newState
        
        if let timeout = self.timeout {
            timeout.invalidate()
        }
        
        self.timeout = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { (timer) in
            print("Timeout")
            self.spinner.startAnimating()
            self.label.text = ""
            self.badge.text = ""
            self.badge.isHidden = true
        }
    }
    
    func parse(_ manufacturer: Data) {
        let key = manufacturer.subdata(in: 2..<manufacturer.count)
        
        let REC = Data(bytes: [0x52, 0x45, 0x43, 0x00, 0x04])
        let PAIR = Data(bytes: [0x30, 0x34, 0x30])
        
        if (key == REC) {
            self.state = .recording
        } else if (key == PAIR) {
            self.state = .pairing
        } else if (key.count == 3) {
            self.state = .snapcode
        } else {
            self.state = .idle
        }
    }
}
